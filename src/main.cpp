#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>

#include "main.h"

const int BUFFER_SIZE = 512;

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
	glfwWindowHint(GLFW_RESIZABLE, false);
	// glfwWindowHint(GLFW_DECORATED, false);
	GLFWwindow *window = glfwCreateWindow(800, 600, "OpenGL", NULL, NULL);
	glfwMakeContextCurrent(window);
	glewExperimental = true;
	glewInit();


	GLuint vbo;
	glGenBuffers(1, &vbo);
	// std::cout << "vertex buffer: " << vbo << std::endl;
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	float vertices[] =
		{-0.5f, -0.5f,
		 0.5f, -0.5f,
		 0.0f, 0.5f};
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertShaderContent;
	getShaderFile("../src/shader.vert", &vertShaderContent);
	const GLchar *vertexShaderSource = vertShaderContent.c_str();
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);

	GLint vertexShaderStatus;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &vertexShaderStatus);
	char vertexShaderLogBuffer[BUFFER_SIZE];
	glGetShaderInfoLog(vertexShader, BUFFER_SIZE, NULL, vertexShaderLogBuffer);
	std::cout << "vertex shader status: " << vertexShaderStatus << std::endl;
	std::cout << "vertex shader log: " << vertexShaderLogBuffer << std::endl;


	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragmentShaderContent;
	getShaderFile("../src/shader.frag", &fragmentShaderContent);
	const GLchar *fragmentShaderSource = fragmentShaderContent.c_str();
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);

	GLint fragmentShaderStatus;
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &fragmentShaderStatus);
	char fragmentShaderLogBuffer[BUFFER_SIZE];
	glGetShaderInfoLog(fragmentShader, BUFFER_SIZE, NULL, fragmentShaderLogBuffer);
	std::cout << "fragment shader status: " << fragmentShaderStatus << std::endl;
	std::cout << "fragment shader log: " << fragmentShaderLogBuffer << std::endl;

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);


	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, false, 0, 0);

	std::cout << "gl error: " << glGetError();

	while (!glfwWindowShouldClose(window))
	{
		glDrawArrays(GL_TRIANGLES, 0, 3);

		glfwSwapBuffers(window);
		glfwPollEvents();
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		{
			std::this_thread::sleep_for(std::chrono::seconds(1));
			glfwSetWindowShouldClose(window, true);
		}
	}
	glfwTerminate();
	return 0;
}

void getShaderFile(std::string filePath, std::string *content)
{
	std::ifstream inFile;
	inFile.open(filePath);
	std::string line;
	while (getline(inFile, line)) content->append(line + '\n');
	inFile.close();
}